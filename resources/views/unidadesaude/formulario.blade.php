@extends('layouts.template')

@section('content')

@include('messages.alert')


@if($unidadesaude->exists)

<div class="col-12 text-center">
  <h3>EDITAR UNIDADE DE SAÚDE</h3>
</div>

<form action="{{route('unidadesaude.update', $unidadesaude)}}" method="post">
  <input type="hidden" name="_method" value="put">

  @else

  <div class="col-12 text-center">
    <h3>CADASTRO DE UNIDADE DE SAÚDE</h3>
  </div>
 
  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>
  @endif

  <form action="{{route('unidadesaude.store')}}" method="post">
    @endif
    {{ csrf_field() }}
    <div class="row">
      <div class="form-group col-6">
        <label>Nome *</label>
        <input name="nome" id="nome" value="{{old('descricao',$unidadesaude->nome)}}" type="text" class="form-control" placeholder="Digite o nome da unidade de saúde" required />
      </div>
      <div class="form-group col-6">
        <label>CNES *</label>
        <input name="cnes" id="cnes" value="{{old('cnes',$unidadesaude->cnes)}}" type="text" class="form-control" placeholder="Digite o código cnes" required />
      </div>
      <div class="form-group col-6">
        <label>Tipo de Prestação *</label>
        <select class="form-control mb-3" name="tipo_prestacao_id" id="tipo_prestacao_id" required>
          <option value="" selected>-- Selecione --</option>
          @foreach ($tipo_prestacao as $tp)
          <option value="{{$tp->id}}" {{$tp->id == $unidadesaude->tipo_prestacao_id?"selected":""}}>{{$tp->descricao}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-6">
        <label>Tipo de Serviço *</label>
        <select class="form-control mb-3" name="tipo_servico_id" id="tipo_servico_id" required>
          <option value="" selected>-- Selecione --</option>
          @foreach ($tipo_servico as $ts)
          <option value="{{$ts->id}}" {{$ts->id == $unidadesaude->tipo_servico_id?"selected":""}}>
            {{$ts->descricao}}
          </option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-6">
        <label>Referência Covid *</label>
        <select class="form-control mb-3" name="referencia_covid" id="referencia_covid" required>
          <option value="" selected>-- Selecione --</option>
          <option value="1" {{(old('referencia_covid', $unidadesaude->referencia_covid) == '1'?'selected':'')}}>Sim</option>
          <option value="2" {{(old('referencia_covid', $unidadesaude->referencia_covid) == '2'?'selected':'')}}>Não</option>
        </select>
      </div>
      <div class="form-group col-6">
        <label>Município *</label>
        <select class="form-control mb-3" name="municipio_id" id="municipio_id" required>
          <option value="" selected>-- Selecione --</option>
          @foreach ($municipio_id as $mi)
          <option value="{{$mi->id}}" {{$mi->id == $unidadesaude->municipio_id?"selected":""}}>{{$mi->nome}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="row">
      <div class="form-group form-footer col-12 text-center" style="margin-bottom: 50px">

        <button class="btn btn-primary" type="submit">Salvar</button>

        <a class="btn btn-default" href="{{route('unidadesaude.index')}}">
          Cancelar
        </a>
      </div>
    </div>

  </form>
  @endsection