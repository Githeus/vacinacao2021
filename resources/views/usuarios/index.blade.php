@extends('layouts.template')
@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <h1>
                Gerenciamento de usuários
            </h1>
        </div>

        <div class="col-12 text-center my-3">
            <a href="{{route('usuario.create')}}" class="btn btn-primary">
                Novo usuário
            </a>
        </div>
        <div class="col-12 col-md-8">
            @include('messages.error')
        </div>

        <div class="col-12 col-md-8">
            <table class="table table-bordered shadow">
                <thead>
                    <tr class="bg-dark text-white text-center">
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Unidade</th>
                        <th>Perfil</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $u)
                    <tr>
                        <td>{{$u->name}}</td>
                        <td>{{$u->email}}</td>
                        <td>{{$u->unidade->nome}}</td>
                        <td>{{$u->getperfil()}}</td>
                        <td width="10%">
                            <div class="dropdown open">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown">
                                    Selecionar
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{route('usuario.edit',$u->id)}}">
                                        Editar
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $('.table').DataTable({
            "searching": true,
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
        });
        @if(session('success'))
            alert({{session('success')}});
        @endif
    </script>
@endsection