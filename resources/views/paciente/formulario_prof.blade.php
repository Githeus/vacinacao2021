<div class="row">
    <div class="col-12 text-center">
        <h3>Dados do Profissional</h3>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <label for="unidade_id" class="control-label">Unidade<span>*</span></label>
        <select class="form-control" id="unidade_id" name="unidade_id">
            <option value="" selected>---- Selecione ----</option>
            @foreach($unidades as $u)
            @if($paciente->pacienteunidade)
            <option value="{{$u->id}}" {{(old('unidade_id', $paciente->pacienteunidade->unidade_id) == $u->id?'selected':'')}}>{{$u->nome}}</option>
            @else
            <option value="{{$u->id}}" {{(old('unidade_id') == $u->id?'selected':'')}}>{{$u->nome}}</option>
            @endif
            @endforeach
        </select>
    </div>

    <div class="col-6">
        <label for="setor_trabalho_id" class="control-label">Setor de Trabalho<span>*</span></label>
        <select class="form-control" id="setor_trabalho_id" name="setor_trabalho_id">
            <option value="" selected>---- Selecione ----</option>
            @foreach($setor_trabalho as $s)
            @if($paciente->pacienteunidade)
            <option value="{{$s->id}}" {{(old('setor_trabalho_id', $paciente->pacienteunidade->setor_trabalho_id) == $s->id?'selected':'')}}>{{$s->descricao}}</option>
            @else
            <option value="{{$s->id}}" {{(old('setor_trabalho_id') == $s->id?'selected':'')}}>{{$s->descricao}}</option>
            @endif

            @endforeach
        </select>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <label>Setor Atuação<span>*</span></label>
        <input name="setor_atuacao" id="setor_atuacao" placeholder="Nome Setor Atuação" class="form-control mb-3" value="{{old('setor_atuacao', $paciente->pacienteunidade?$paciente->pacienteunidade->setor_atuacao:'')}}" />
    </div>

    <div class="col-4">
        <label for="cargo_id" class="control-label">Cargo</label>
        <select class="form-control" id="cargo_id" name="cargo_id">
            <option value="" selected>---- Selecione ----</option>
            @foreach($cargos as $c)
            @if($paciente->pacienteunidade)
            <option value="{{$c->id}}" {{(old('cargo_id', $paciente->pacienteunidade->cargo_id) == $c->id?'selected':'')}}>{{$c->descricao}}</option>
            @else
            <option value="{{$c->id}}" {{(old('cargo_id') == $c->id?'selected':'')}}>{{$c->descricao}}</option>
            @endif
            @endforeach
        </select>
    </div>
    <div class="col-4">
        <label>Vínculo com outro estabelecimento de saúde? <span>*</span></label>
        <select class="form-control mb-3" name="vinculo_us" id="vinculo_us">
            <option value="" selected>-- Selecione --</option>
            <option value="1" {{(old('vinculo_us', $paciente->vinculo_us) == '1'?'selected':'')}}>Sim</option>
            <option value="0" {{(old('vinculo_us', $paciente->vinculo_us) == '0'?'selected':'')}}>Não</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <label for="vinculo_id" class="control-label">Vínculo</label>
        <select class="form-control" id="vinculo_id" name="vinculo_id">
            <option value="" selected>---- Selecione ----</option>
            @foreach($vinculos as $v)
            @if($paciente->pacienteunidade)
            <option value="{{$v->id}}" {{(old('vinculo_id', $paciente->pacienteunidade->vinculo_id) == $v->id?'selected':'')}}>{{$v->descricao}}</option>
            @else
            <option value="{{$v->id}}" {{(old('vinculo_id') == $v->id?'selected':'')}}>{{$v->descricao}}</option>
            @endif
            @endforeach
        </select>
    </div>

    <div class="col-6">
        <label for="turno_id" class="control-label">Turno</label>
        <select class="form-control" id="turno_id" name="turno_id">
            <option value="" selected>---- Selecione ----</option>
            @foreach($turnos as $t)
            @if($paciente->pacienteunidade)
            <option value="{{$t->id}}" {{(old('turno_id', $paciente->pacienteunidade->turno_id) == $t->id?'selected':'')}}>{{$t->descricao}}</option>
            @else
            <option value="{{$t->id}}" {{(old('turno_id') == $t->id?'selected':'')}}>{{$t->descricao}}</option>
            @endif
            @endforeach
        </select>
    </div>
</div>
