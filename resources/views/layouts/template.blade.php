<!DOCTYPE html>
<html lang="pt-br">

<head>
	<script src="{{asset('js/app.js')}}"></script>
	<script src="{{asset('js/vacina.js')}}"></script>

	<link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link href="{{asset('css/vacina.css')}}" rel="stylesheet">

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>COVID-19 Controle de Vacinação</title>
	@stack('script')
	@stack('style')
</head>

<body>

	<div class="container-fluid">
		<div class="row">
			<img class="col-12 img-fluid" src="{{asset('img/banner.jpeg')}}">
			@auth
			<div class="col-12">
				<nav class="navbar navbar-expand-sm navbar-light bg-light shadow-sm mb-2">
					<a class="navbar-brand" href="/">
						Controle de Vacinação COVID-19
					</a>
					<button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId">
						<span class="navbar-toggler-icon"></span>
					</button>


					{{--<div class="collapse navbar-collapse" id="collapsibleNavId">
						<ul class="navbar-nav mx-auto mt-2 mt-lg-0">--}}
                    <div class="collapse navbar-collapse" id="collapsibleNavId">
                        <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                            <li></li>
                            <li class="dropdown active">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Cadastros <span class="caret"></span></a>
                                <ul class="dropdown-menu">

                            @if(auth()->user()->perfil == 1)

                            <li class="nav-item active">
                                <a class="nav-link" href="{{url('cargo')}}">Cargos</a>
                            </li>
							<li class="nav-item active">
								<a class="nav-link" href="{{url('comorbidade')}}">Comorbidades</a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="{{url('tipoServico')}}">Tipo de Serviço</a>
							</li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{url('unidadesaude')}}">Unidades</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="{{url('vinculo')}}">Vínculo</a>
                            </li>
							@endif

                                </ul>
                            </li> {{--dropdown cadastros--}}

							@if(auth()->user()->perfil == 1 || auth()->user()->perfil == 2)
							<li class="nav-item active">
								<a class="nav-link" href="{{route('usuario.index')}}">Usuários</a>
							</li>
							@endif
							<li class="nav-item active">
								<a class="nav-link" href="{{url('paciente')}}">Paciente</a>
							</li>

						</ul>


						<form method="post" action="{{ route('logout') }}" class="form-inline my-2 my-lg-0">
							{{csrf_field()}}
							<span class="px-2 text-center">
								{{auth()->user()->name}} -
								{{auth()->user()->getperfil()}}
							</span>
							<button type="submit" class='btn btn-danger btn-sm'>
								Sair do sistema <i class="fas fa-sign-out-alt"></i>
							</button>
						</form>
					</div>
				</nav>

			</div>
			@endauth
		</div>
		<div class="row">
			<div class="col-12">
				@yield('content')
			</div>
		</div>
	</div>
</body>

</html>
