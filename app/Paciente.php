<?php

namespace App;

use App\Utils\StringsUtil;
use App\Utils\ValidaCpfCnpj;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Paciente extends Model
{
    protected $table = 'pacientes';
    public $fillable = [
        'id','nome','cpf', 'sexo','data_nascimento','unidadecadastro_id', 'vacinado', 'fase_vacinacao',
        'profissional_saude', 'vinculo_us', 'raca_id', 'cns', 'municipio_id', 'user_id'
    ];

    // $paciente->sexo = 1 ou 2
    // $paciente->sexo() = Fem ou masc
    public function getsexo(){
        switch($this->sexo){
            case 1: return "Masculino";break;
            case 2: return "Feminino";break;

        }
    }

    public function getprofissionalsaude(){
        switch($this->profissional_saude){
            case 1: return "Sim";break;
            case 0: return "Não";break;

        }
    }

    public function getvinculous(){
        switch($this->vinculo_us){
            case 1: return "Sim";break;
            case 0: return "Não";break;
        }
    }

    public function cargo(){
        return $this->belongsTo('App\Cargo');
    }

    public function raca(){
        return $this->belongsTo('App\Raca');
    }

    public function vinculo(){
        return $this->belongsTo('App\Vinculo');
    }

    public function setor_trabalho(){
        return $this->belongsTo('App\SetorTrabalho');
    }

    public function pacienteunidade(){

        return $this->hasOne('App\PacienteUnidade', 'paciente_id', 'id');

    }

    public function municipio(){

        return $this->belongsTo('App\Municipio');

    }

    public function comorbidade(){
        return $this->belongsToMany('App\Comorbidade', 'paciente_comorbidade', 'paciente_id', 'comorbidade_id');
    }

    // Form edit select
    public function hasComorbidade($comorbidade_id){
        $resultado = DB::table('paciente_comorbidade')
                        ->where('paciente_id',$this->id)
                        ->where('comorbidade_id',$comorbidade_id)
                        ->first();
        return $resultado?"selected":"";
    }

    //  Return boolean
    public function hasComorbidadePaciente($comorbidade_id){
        $resultado = DB::table('paciente_comorbidade')
                        ->where('paciente_id',$this->id)
                        ->where('comorbidade_id',$comorbidade_id)
                        ->first();
        return $resultado;
    }


    /*public function comorbidadeBD(){

        $arrayListaBD = array();

        foreach($this->comorbidade as $c => $value){
            $arrayListaBD[$c] = $value['id'];
        }

    }*/


    public function unidade_cadastro(){
        return $this->belongsTo('App\UnidadeSaude','unidadecadastro_id');
    }

    public function DataNascimento(){
        return date('d/m/Y',strtotime($this->data_nascimento));
    }

    public function CPFvalido($cpf){

        $cpf = new ValidaCpfCnpj($cpf);

        if(!$cpf->valida()){

            return false;
        }

        return true;

   }


    //verifica se já existe cpf cadastrado
    public function existeCPFcadastrado($cpf, $id){

        $cpf = StringsUtil::limpaCPF_CNPJ($cpf);

        if($id){//update

            $existeCPF = DB::table('pacientes')->where([
                ['cpf', '=', $cpf],
                ['id', '<>', $id],
            ])->count();

        }else{//store
            $existeCPF = DB::table('pacientes')->where([
                ['cpf', '=', $cpf],
            ])->count();
        }

        if($existeCPF > 0){
            return true;
        }else{
            return false;
        }

    }

    //verifica se já existe cns cadastrado
    public function existeCNScadastrado($cns, $id){


        if($id){//update

            $existeCNS = DB::table('pacientes')->where([
                ['cns', '=', $cns],
                ['id', '<>', $id],
            ])->count();

        }else{//store
            $existeCNS = DB::table('pacientes')->where([
                ['cns', '=', $cns],
            ])->count();
        }

        if($existeCNS > 0){
            return true;
        }else{
            return false;
        }

    }

    public function getIdade($dataNasc){

        $date = new DateTime($dataNasc);
        $interval = $date->diff(new DateTime(date('Y-m-d')));
        return $interval->format('%Y');
    }
}
