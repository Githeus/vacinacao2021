<?php

namespace App\Http\Middleware;

use Closure;

class VerificaPerfil
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$modulo)
    {
        // Associa os módulos a variavel autorização
        switch($modulo){
            case "comorbidades": $authorized = [1]; break;
            case "cargos": $authorized = [1]; break;
            case "vinculos": $authorized = [1]; break;
            case "unidades": $authorized = [1]; break;
            case "tipo_servicos": $authorized = [1]; break;
            case "users": $authorized = [1,2]; break;
        }
        // Verifica se perfil do usuário está nas autorizações
        if(!in_array( auth()->user()->perfil , $authorized ))
            return abort(403,"Acesso não permitido, verificar seu perfil");
        return $next($request);
    }
}
