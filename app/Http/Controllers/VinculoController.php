<?php

namespace App\Http\Controllers;

use App\Vinculo;
use Illuminate\Http\Request;
use Request as rq;

class VinculoController extends Controller
{
    public function __construct(){
        $this->middleware('verificaperfil:vinculos');
    }
    public function index(){

        $vinculo = Vinculo::orderBy('descricao')->get();

        return view('vinculo.index', compact('vinculo'));
    }

    public function create(){

        $vinculo = new Vinculo();

        return view('vinculo.formulario', compact('vinculo'));
    }

    public function store(Request $request){

        $vinculo = new Vinculo();
        $vinculo->descricao = $request->descricao;

        try {
            $save = $vinculo->save();

            if($save){
                rq::session()->flash('status', 'Vínculo cadastrado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar vínculo.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar vínculo.'.$e->getMessage());
        }

        return redirect()->route('vinculo.index');
    }

    public function edit(Vinculo $vinculo){

        return view('vinculo.formulario', compact('vinculo'));
    }

    public function update(Request $request, Vinculo $vinculo){

        $vinculo->descricao = $request->descricao;

        try {
            $save = $vinculo->save();

            if($save){
                rq::session()->flash('status', 'Vínculo atualizado com sucesso.');
            }else{
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar vínculo.');
            }

        }catch (\Exception $e){
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar vínculo.'.$e->getMessage());
        }

        return redirect()->route('vinculo.index');
    }

    public function delete($id)
    {
        $vinculo = Vinculo::findOrFail($id);

        try{
            $vinculo->delete();
            rq::session()->flash('status', 'Vínculo '.$vinculo->descricao.' excluído com sucesso.');
            return redirect()->route('vinculo.index');
        }
        catch (\Exception $e){

            if($e->getCode() == 23000 ){
                rq::session()->flash('status-not', 'Vínculo não foi excluído. O mesma está sendo utilizado em outro cadastro! ');
            }else{
                rq::session()->flash('status-not', 'Vínculo não foi excluído! '.$e->getMessage());
            }

            return redirect()->route('vinculo.index');
        }

    }
}
