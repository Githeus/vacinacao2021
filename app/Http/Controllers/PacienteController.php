<?php

namespace App\Http\Controllers;

use App\Cargo;
use App\Comorbidade;
use App\Http\Requests\PacienteRequest;
use App\Municipio;
use App\Paciente;
use App\PacienteComorbidade;
use App\PacienteUnidade;
use App\Raca;
use App\SetorTrabalho;
use App\TipoServico;
use App\Turno;
use App\UnidadeSaude;
use App\Utils\StringsUtil;
use App\Vinculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Request as rq;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PacienteController extends Controller
{
    public function index(){

        $pacientes = Paciente::get();

        return view('paciente.index', compact('pacientes'));
    }

    public function create(){

        $paciente = new Paciente();

        $cargos = Cargo::orderBy('descricao')->get();

        $vinculos = Vinculo::orderBy('descricao')->get();

        $unidades = UnidadeSaude::orderBy('nome')->get();

        $setor_trabalho = SetorTrabalho::orderBy('descricao')->get();

        $turnos = Turno::orderBy('descricao')->get();

        $racas = Raca::orderBy('descricao')->get();

        $municipios = Municipio::where('estado_id', 3)->orderBy('nome')->get();

        $comorbidades = Comorbidade::orderBy('descricao')->get();


        return view('paciente.formulario', compact('paciente', 'cargos', 'vinculos', 'unidades', 'setor_trabalho', 'turnos', 'racas',
                 'municipios', 'comorbidades'));
    }

    public function store(PacienteRequest $request){


        $paciente = new Paciente();

        $vinculo_us = $request->vinculo_us;

        //validar CPF de paciente
        if(!$paciente->CPFvalido($request->cpf, null)){

            rq::session()->flash('status-not', 'CPF inválido.');
            return back()->withInput();
        }

        //verificar se já existe registro com cpf informado
        if($paciente->existeCPFcadastrado($request->cpf, null)){

            rq::session()->flash('status-not', 'O registro não foi salvo. Paciente com Cpf já cadastrado no sistema.');
            return back()->withInput();
        }

        //verificar se já existe registro com cns informado
        if($request->cns && $paciente->existeCNScadastrado($request->cns, null)){

            rq::session()->flash('status-not', 'O registro não foi salvo. Paciente com CNS já cadastrado no sistema.');
            return back()->withInput();
        }


        $profissional = $request->ckProfSaude;

         if($profissional){
             $profissional = 1;

             $validator = Validator::make($request->all(), [
                'unidade_id' => 'required',
                'vinculo_us' => 'required',
                'setor_trabalho_id' => 'required',
                'setor_atuacao' => 'required',
             ],
              [
                'unidade_id.required' => 'O campo Unidade é obrigatório.',
                'vinculo_us.required' => 'O campo Vínculo com outro estabelecimento é obrigatório.',
                'setor_trabalho_id.required' => 'O campo Setor de Trabalho é obrigatório.',
                'setor_atuacao.required' => 'O campo Setor de Atuação é obrigatório.',

              ]

        );

            if ($validator->fails()) {
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }


         }else{
             $profissional = 0;
             $vinculo_us = null;
         }



        DB::beginTransaction();
        try {

            $save = Paciente::create([
                'nome' => $request->nome,
                'cpf' => StringsUtil::limpaCPF_CNPJ($request->cpf),
                'data_nascimento' => $request->data_nascimento,
                'sexo' => $request->sexo,
                'vacinado' => '0', //default
                'fase_vacinacao' => $request->fase_vacinacao,
                'profissional_saude' => $profissional,
                'unidadecadastro_id' => Auth::user()->unidade_id,
                'vinculo_us' => $vinculo_us,
                'cns' => $request->cns,
                'raca_id' => $request->raca_id,
                'municipio_id' => $request->municipio_id,
                'user_id' => Auth::user()->id
            ]);

            //Comorbidade
            $saveComorbidade = null;
            $comorbidadesarray = $request->comorbidades;
            $comorbidades = array();

            if($comorbidadesarray){

                foreach($comorbidadesarray as $key => $com){

                    $comorbidades[$key] = array(
                        'paciente_id'=>$save->id,
                        'comorbidade_id'=>$com,
                    );
                }

                $saveComorbidade = DB::table('paciente_comorbidade')->insert($comorbidades);

            }

            //Dados do Profissional
            $saveProfissional = null;

            if($profissional){

                $saveProfissional = PacienteUnidade::create([
                    'paciente_id' =>  $save->id,
                    'unidade_id' => $request->unidade_id,
                    'setor_trabalho_id' => $request->setor_trabalho_id,
                    'cargo_id' => $request->cargo_id,
                    'vinculo_id' => $request->vinculo_id,
                    'turno_id' => $request->turno_id,
                    'setor_atuacao' => $request->setor_atuacao

                ]);


            }


            if($save && (!$profissional || $profissional && $saveProfissional)
                && (!$comorbidadesarray || $comorbidadesarray && $saveComorbidade)
            ){

                Log::channel('relatoria')->info("Paciente cadastrado",[
                    'user_id'=>auth()->user()->id,
                    'dados_paciente'=>$save,
                    'dados_profissional'=>$saveProfissional
                ]);

                DB::commit();
                rq::session()->flash('status', 'Paciente cadastrado com sucesso.');
            }else{
                DB::rollBack();
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar paciente.');
            }

        }catch (\Exception $e){
            DB::rollBack();
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar paciente. '.$e->getMessage());
        }

        return redirect()->route('paciente.index');
    }

    public function edit(Paciente $paciente){

        $cargos = Cargo::orderBy('descricao')->get();

        $vinculos = Vinculo::orderBy('descricao')->get();

        $unidades = UnidadeSaude::orderBy('nome')->get();

        $setor_trabalho = SetorTrabalho::orderBy('descricao')->get();

        $turnos = Turno::orderBy('descricao')->get();

        $racas = Raca::orderBy('descricao')->get();

        $municipios = Municipio::where('estado_id', 3)->orderBy('nome')->get();

        $comorbidades = Comorbidade::orderBy('descricao')->get();

        return view('paciente.formulario', compact('paciente', 'cargos', 'vinculos', 'unidades', 'setor_trabalho', 'turnos', 'racas',
                 'municipios', 'comorbidades'));

    }


    public function update(PacienteRequest $request, Paciente $paciente){

        //Campos para o log
        $oldpaciente = Paciente::find($paciente->id);
        $oldprofissional = PacienteUnidade::where('paciente_id', $paciente->id)->first();


        $vinculo_us = $request->vinculo_us;

        //validar CPF de paciente
        if(!$paciente->CPFvalido($request->cpf, null)){

            rq::session()->flash('status-not', 'CPF inválido.');
            return back()->withInput();
        }

        //verificar se já existe registro com cpf informado
        if($paciente->existeCPFcadastrado($request->cpf, $paciente->id)){

            rq::session()->flash('status-not', 'O registro não foi salvo. Paciente com Cpf já cadastrado no sistema.');
            return back()->withInput();
        }

         //verificar se já existe registro com cns informado
         if($request->cns && $paciente->existeCNScadastrado($request->cns, $paciente->id)){

            rq::session()->flash('status-not', 'O registro não foi salvo. Paciente com CNS já cadastrado no sistema.');
            return back()->withInput();
        }

        //Dados do profissional
        $profissional = $request->ckProfSaude;

         if($profissional){
             $profissional = 1;
         }else{
             $profissional = 0;
             $vinculo_us = null;
         }

         //validar campos em dados profissional
         if($profissional){

                $validator = Validator::make($request->all(), [
                'unidade_id' => 'required',
                'vinculo_us' => 'required',
                'setor_trabalho_id' => 'required',
                'setor_atuacao' => 'required',
                ],
                [
                'unidade_id.required' => 'O campo Unidade é obrigatório.',
                'vinculo_us.required' => 'O campo Vínculo com outro estabelecimento é obrigatório.',
                'setor_trabalho_id.required' => 'O campo Setor de Trabalho é obrigatório.',
                'setor_atuacao.required' => 'O campo Setor de Atuação é obrigatório.',
                ]

            );

            if ($validator->fails()) {
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }


         }


        try {

            $save = $paciente->update([
                'nome' => $request->nome,
                'cpf' => StringsUtil::limpaCPF_CNPJ($request->cpf),
                'data_nascimento' => $request->data_nascimento,
                'sexo' => $request->sexo,
                'fase_vacinacao' => $request->fase_vacinacao,
                'profissional_saude' => $profissional,
                'vinculo_us' => $vinculo_us,
                'cns' => $request->cns,
                'raca_id' => $request->raca_id,
                'municipio_id' => $request->municipio_id,
            ]);


            // UPDATE COMORBIDADES
            // 1ª etapa: Deleta comorbidade que não tiver selecionada no edit.
            // dd($paciente->comorbidade->count());
            if($paciente->comorbidade->count() > 0){
                foreach($paciente->comorbidade as $pc){
                    if(is_null($request->comorbidades)){
                        $comorbidade = $paciente->hasComorbidadePaciente($pc->id);
                        DB::table('paciente_comorbidade')->where('id',$comorbidade->id)->delete();
                    }
                    elseif( !array_search($pc->id, $request->comorbidades) ){
                        $comorbidade = $paciente->hasComorbidadePaciente($pc->id);
                        DB::table('paciente_comorbidade')->where('id',$comorbidade->id)->delete();
                    }
                }
            }
            // 2ª etapa: Verifica se tem novas comorbidades
            if(!is_null($request->comorbidades)){
                foreach($request->comorbidades as $com){
                    if(!$paciente->hasComorbidadePaciente($com)){
                        $dados = [
                            'paciente_id'=>$paciente->id,
                            'comorbidade_id'=>$com,
                        ];
                        DB::table('paciente_comorbidade')->insert($dados);
                    }
                }
            }


           //Dados Profissional
           if($oldprofissional){
                $dadosProfissional = PacienteUnidade::where('paciente_id', $paciente->id)->first();
            }else{
                $dadosProfissional = new PacienteUnidade();
                $dadosProfissional->paciente_id = $paciente->id;
            }

            $dadosProfissional->unidade_id = $request->unidade_id;
            $dadosProfissional->setor_trabalho_id = $request->setor_trabalho_id;
            $dadosProfissional->cargo_id = $request->cargo_id;
            $dadosProfissional->vinculo_id = $request->vinculo_id;
            $dadosProfissional->turno_id = $request->turno_id;
            $dadosProfissional->setor_atuacao = $request->setor_atuacao;

             //nada a alterar em profissional
            if(!$oldprofissional && !$profissional){

                $saveProfissional = true;
            }
            if($oldprofissional && !$profissional){
                //delete dados prof
                $saveProfissional =  $dadosProfissional->delete();
                $dadosProfissional = null; //para log

            }
            //senao cria ou atualiza
            elseif($profissional){
                $saveProfissional = $dadosProfissional->save();

            }


            if($save && $saveProfissional){

                Log::channel('relatoria')->info("Paciente alterado",[
                    'user_id'=>auth()->user()->id,
                    'dados_paciente_old'=>$oldpaciente,
                    'dados_profissional_old'=>$oldprofissional,
                    'alterações'=>$paciente,
                    'alterações_profissional'=>$dadosProfissional
                ]);

                DB::commit();
                rq::session()->flash('status', 'Paciente atualizado com sucesso.');
            }else{
                DB::rollBack();
                rq::session()->flash('status-not', 'Ocorreu um erro ao salvar paciente.');
            }

        }catch (\Exception $e){
            DB::rollBack();
            rq::session()->flash('status-not', 'Ocorreu um erro ao salvar paciente. '.$e->getMessage());
        }

        return redirect()->route('paciente.index');
    }

    public function visualizacao(Paciente $paciente){
        return view('paciente.visualizacao',compact('paciente'));
    }




}
