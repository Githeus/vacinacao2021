<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PacienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'=>'required',
            'sexo'=>'required',
            'cpf'=>'required',
            'data_nascimento'=>'required',
            'fase_vacinacao'=>'required'
        ];
    }

    public function messages()
    {
        return [            
            'nome.required' => 'O campo Nome é de preenchimento obrigatório!',
            'sexo.required' => 'O campo Sexo é de preenchimento obrigatório!',
            'cpf.required' => 'O campo CPF é de preenchimento obrigatório!',
            'data_nascimento.required' => 'O campo Data de Nascimento é de preenchimento obrigatório!',
            'fase_vacinacao.required' => 'O campo Fase de Vacinação é de preenchimento obrigatório!',
        ];
    }
}
