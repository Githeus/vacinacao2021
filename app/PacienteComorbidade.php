<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PacienteComorbidade extends Model
{
    protected $table = 'paciente_comorbidade';
}
