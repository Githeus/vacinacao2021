<?php

use Database\Seeders\ComorbidadeSeeder;
use Database\Seeders\EstadoSeeder;
use Database\Seeders\RacaSeeder;
use Database\Seeders\SetorTrabalhoSeeder;
use Database\Seeders\TipoPrestacaoSeeder;
use Database\Seeders\TipoServicoSeeder;
use Database\Seeders\TurnoSeeder;
use Database\Seeders\CargoSeeder;
use Database\Seeders\VinculoSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ComorbidadeSeeder::class);
        $this->call(SetorTrabalhoSeeder::class);
        $this->call(TipoPrestacaoSeeder::class);
        $this->call(TipoServicoSeeder::class);
        $this->call(TurnoSeeder::class);
        $this->call(RacaSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CargoSeeder::class);
        $this->call(VinculoSeeder::class);
    }
}
