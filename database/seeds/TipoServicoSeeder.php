<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_servico')->insert([
            'descricao' => 'Maternidade'         
        ]);

        DB::table('tipo_servico')->insert([
            'descricao' => 'Público'         
        ]);

   
      
    }
}
