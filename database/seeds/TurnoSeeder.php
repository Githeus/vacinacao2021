<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TurnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('turnos')->insert([
            'descricao' => 'Noturno'         
        ]);

        DB::table('turnos')->insert([
            'descricao' => 'Diurno'         
        ]);

        DB::table('turnos')->insert([
            'descricao' => 'Matutino'         
        ]);

        DB::table('turnos')->insert([
            'descricao' => 'Vespertino'         
        ]);

   
      
    }
}
