<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargos')->insert([
            'descricao' => 'Auxiliar de Serviços Gerais'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Auxiliar Administrativo'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Auxiliar de Enfermagem'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Bioquímica'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Enfermeiro(a)'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Enfermeiro(a) Intensivista'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Fisioterapeuta'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Fonoaudiólogo(a)'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Gerente'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Ginecologista/Obstetra'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Intensivista Clínico'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Maqueiro'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Médico(a)'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Médico(a) Anestesista'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Médico(a) Cirurgião'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Médico(a) Pediatra'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Técnico(a) de Enfermagem'
        ]);

        DB::table('cargos')->insert([
            'descricao' => 'Técnico(a) Patologia'
        ]);


    }
}
