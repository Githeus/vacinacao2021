<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VinculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vinculos')->insert([
            'descricao' => 'Cooperativa'
        ]);

        DB::table('vinculos')->insert([
            'descricao' => 'Contratação direta'
        ]);

        DB::table('vinculos')->insert([
            'descricao' => 'Estatutário'
        ]);

        DB::table('vinculos')->insert([
            'descricao' => 'Terceirizado'
        ]);

    }
}
