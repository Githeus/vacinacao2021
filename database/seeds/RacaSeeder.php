<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RacaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('racas')->insert([
            'descricao' => 'Branco'         
        ]);

        DB::table('racas')->insert([
            'descricao' => 'Preto'         
        ]);

        DB::table('racas')->insert([
            'descricao' => 'Pardo'         
        ]);

        DB::table('racas')->insert([
            'descricao' => 'Amarelo'         
        ]);

        DB::table('racas')->insert([
            'descricao' => 'Indígena'         
        ]);

   
      
    }
}
