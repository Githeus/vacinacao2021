<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnidadeSaudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cnes')->unique();
            
            $table->bigInteger('tipo_prestacao_id')->unsigned()->nullable();
            $table->foreign('tipo_prestacao_id')->references('id')->on('tipo_prestacao');
            
            $table->bigInteger('tipo_servico_id')->unsigned()->nullable();
            $table->foreign('tipo_servico_id')->references('id')->on('tipo_servico');
            
            $table->boolean('referencia_covid');
            
            $table->bigInteger('municipio_id')->unsigned()->nullable();
            $table->foreign('municipio_id')->references('id')->on('municipios');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
    }
}
