<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome');
            $table->string('cpf')->unique();
            $table->string('cns', 20)->nullable();
            $table->tinyInteger('sexo')->nullable()->comment('1-Masculino 2-Feminino');
            $table->date('data_nascimento');
            $table->boolean('vacinado');
            $table->tinyInteger('fase_vacinacao');
            $table->boolean('profissional_saude');
            $table->tinyInteger('vinculo_us')->nullable()->comment('1-Sim 0-Não');
            
            $table->bigInteger('raca_id')->unsigned()->nullable();
            $table->foreign('raca_id')->references('id')->on('racas');
            
            $table->bigInteger('municipio_id')->unsigned()->nullable()->comment('Município de residência.');
            $table->foreign('municipio_id')->references('id')->on('municipios');            

            $table->bigInteger('unidadecadastro_id')->unsigned()->nullable()->comment('Unidade de cadastro');
            $table->foreign('unidadecadastro_id')->references('id')->on('unidades');

            $table->bigInteger('user_id')->unsigned()->nullable()->comment('Usuário de cadastro');
            $table->foreign('user_id')->references('id')->on('users');


            $table->timestamps();
            
          

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
