<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/primeiroacesso', 'HomeController@primeiroacesso')->name('primeiroacesso')->middleware('auth');
Route::post('/primeiroacesso', 'HomeController@update')->name('primeiroacesso.update')->middleware('auth');

// ROTAS AUTENTICADAS
Route::middleware(['auth','primeiroacesso'])->group(function () {

    Route::redirect('/', '/home');
    Route::get('/home', 'HomeController@index')->name('home');

    //Usuários
    Route::resource('usuario', 'UserController');

    //Comorbidade
    Route::resource('comorbidade', 'ComorbidadeController');
    Route::get('/comorbidade/remove/{id}', 'ComorbidadeController@delete');

    //Cargos
    Route::resource('cargo', 'CargoController');
    Route::get('/cargo/remove/{id}', 'CargoController@delete');

    //Tipo de serviço
    Route::resource('tipoServico', 'TipoServicoController');
    Route::get('/tipo-servico/remove/{id}', 'TipoServicoController@delete');

    //Unidades de Saúde
    Route::resource('unidadesaude', 'UnidadeSaudeController');
    Route::get('/unidadesaude/remove/{id}', 'UnidadeSaudeController@delete');

    //Pacientes
    Route::resource('paciente', 'PacienteController');
    Route::get('visualizacao/{paciente}','PacienteController@visualizacao')->name('visualizacao');

    //Vínculo
    Route::resource('vinculo', 'VinculoController');
    Route::get('/vinculo/remove/{id}', 'VinculoController@delete');


});
